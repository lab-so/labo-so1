#include <unistd.h>

#include <sys/wait.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <conio.h>
//#define BC 10
int contador = 0;
//int BC = 0;
char *buffer;
int in = 0, out = 0;
sem_t mutex, vacio, lleno;
int NP, NC, BC;




void *Productor(void *arg){
	
	FILE *arch_p=fopen("Productores.txt", "wt"); 
	
	int *prod = (int *)arg;
    int productor = *prod;
    int contador_productos = 0;
	
	printf("Poductor nº: %d\nEsperando...\n", productor);
	 //while(true){  
	 sleep(1);
		
		sem_wait(&vacio);
		sem_wait(&mutex);
		char producto = 'a';
		contador_productos++;
		fputs("Productor %d generó %c", arch_p);
		buffer[in] = producto;
		in = (in + 1) % BC;
		contador++;
		//printf("\nholaaaa   %c\n", producto);
		printf("Productor %d creó:  %c en posicion In:  %d. Contador: %d \n", productor, producto, in, contador);
	//	printf("Productor %d_%d", number, contador_productos);
		sem_post(&mutex);
		sem_post(&lleno);
	//}
}

void *Consumidor(void *arg){
	
	FILE *arch_c=fopen("Consumidores.txt", "wt");
	
	int *cons = (int *)arg;
    int consumidor = *cons;
    printf("Consumidor nº: %d\nEsperando...", consumidor);
	sleep(1);
	
	//while(true){
		sem_wait(&lleno);
		sem_wait(&mutex);
		
		char producto_consumir = buffer[out];
		out = (out + 1) % BC;
		contador--;
		printf("Consumidor %d consume:  %c en posicion Out:  %d. Contador: %d \n", consumidor, producto_consumir, out, contador);
		sem_post(&mutex);
		sem_post(&vacio);
	//}
}


int main(int argc, char *argv[]){
	
	if(argc == 0){
		printf("No existen parametros para correr el programa");
		return 0;
		
	}
	else{
		for(int i=0; i<argc; i++){
			printf("argumento %d: %s\n", i, argv[i]);
		}
	}
	
	
	
	NP = atoi(argv[1]); // productores
	NC = atoi(argv[2]); // consumidores
	BC = atoi(argv[3]); // buffer
	
	
	
	buffer = (char*)malloc(BC * sizeof(int));
	
	
	
	pthread_t hebras_productoras[NP];
	pthread_t hebras_consumidoras[NC];
	
	
	sem_init(&mutex, 0, 1);
    sem_init(&vacio, 0, BC);
    sem_init(&lleno, 0, 0);
	
	//hebras
	for(int i = 0; i<NP; i++){
     
       int ii = i+1;
       // printf("-------------------------\nproductor: %d\n", ii);
        pthread_create(&hebras_productoras[i], NULL, Productor, (void *)&ii);
       // pthread_join(hebra, NULL);

    }
    
    for(int j = 0; j<NC; j++){
		int jj = j+1;
      
      //  printf("-------------------------\nconsumidor: %d\n", jj);
       pthread_create(&hebras_consumidoras[j], NULL, Productor, (void *)&jj);
       // pthread_join(hebra, NULL);

    }
    
    for(int i=0; i<NP; i++){
		pthread_join(hebras_productoras[i], NULL);
		}
		
	for(int i=0; i<NP; i++){
		pthread_join(hebras_consumidoras[i], NULL);
		}
	for(int i=0; i<BC; i++){
		printf("\n%c", buffer[i]);
	}
	

	printf("%d\n", contador);
	
	
	//Cracion del txt
	FILE *arch=fopen("resultados.txt", "wt"); //wt se encarga de escribir el texto
	if (arch==NULL)
		exit(1);
	fputs("ejercicio.c", arch);
	fclose(arch);
	printf("Se ha creado un archivo de texto con el producto y lo consumido");
	getch();
	
	
	return 0;
}
